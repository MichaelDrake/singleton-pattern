import  XCTest 



//Shared permite a los clientes acceder a la  instancia única de singleton.
class Singleton {


// El campo estático que controla el acceso a la instancia de singleton. 
// Esta implementación le permite extender la clase Singleton mientras mantiene solo una instancia de cada subclase. 

    static var shared: Singleton = {
        let instance = Singleton()
   
        return instance
    }()


    private init() {}

  
    func someBusinessLogic() -> String {
   
        return "Result of the 'someBusinessLogic' call"
    }
}

/// Singletons no debe ser clonable. 
extension Singleton: NSCopying {

    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
// El inicializador de Singleton siempre debe ser privado para evitar las llamadas directa
//Cualquier singleton debe definir alguna lógica de negocios, que puede ejecutarse en su instancia. 

// Codigo en el Cliente 
class Client {
    // ...
    static func someClientCode() {
        let instance1 = Singleton.shared
        let instance2 = Singleton.shared

        if (instance1 === instance2) {
            print("Singleton works, both variables contain the same instance.");
        } else {
            print("Singleton failed, variables contain different instances.");
        }
    }
   
}


class SingletonConceptual: XCTestCase {

    func testSingletonConceptual() {
        Client.someClientCode();
    }
}